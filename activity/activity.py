import binarytree as bt
from binarytree import build

# Create a balanced binary tree with a height of 2
nodes1 = [2, 3, 3, None, 5, 5, None ]
balanced_tree = build(nodes1)

# Print the created balanced tree
print("Balanced Binary Tree: \n", balanced_tree, "\n")

print(f"Is this a balanced tree? {balanced_tree.is_balanced}")
print(f"Is this a symmetrical tree? {balanced_tree.is_symmetric}")
print(f"Height of the balanced tree: {balanced_tree.height}")


# # Create a strict Binary Search Tree
nodes2 = [18, 16, 22, 15, 17, 21, 23]
bst_tree = build(nodes2)

print("BST Binary Tree: \n", bst_tree, "\n")

print(f"Is this BST? {bst_tree.is_bst}")
print(f"Is the BST strict? {bst_tree.is_strict}")
print(f"The min heap is: {bst_tree.min_node_value}")
print(f"The max node value is: {bst_tree.max_node_value}")

nodes3 = [3, 10, 23, 8, 9]
complete_tree = build(nodes3)

print("Complete Tree: \n", complete_tree, "\n")
print(f"Is this a complete tree? {complete_tree.is_complete}")
print(f"The leaf count is: {complete_tree.leaf_count}")
print(f"The levels of the tree: {complete_tree.levels[1]}")

